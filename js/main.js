'use strict';
var eyeTracker = (function() {
  function init() {
    _setUpListeners();
  };
  function _setUpListeners() {
    var eyeTracker = { 
          posY: $('.eye__watcher').position().top,
          posX: $('.eye__watcher').position().left,
          y: $('.eye__watcher').offset().top,
          x: $('.eye__watcher').offset().left
        },
        eyeWidth = $('.eye').width(),
        documentWidth = $(document).width(),
        documentHeight = document.documentElement.clientHeight,
        kX = eyeWidth/documentWidth,
        kY = eyeWidth/documentWidth;
        console.log(documentHeight);
    $(document).on('mousemove', _moveEye);
    function _moveEye(e) {
      var mouseX = e.pageX,
          mouseY = e.pageY,
          deltaX = kX*(eyeTracker.x - mouseX),
          deltaY = kY*(eyeTracker.y - mouseY);
      $('.eye__watcher').css({
        top: eyeTracker.posY - deltaY,
        left: eyeTracker.posX - deltaX
      });
      // console.log($('.eye__watcher').position());
    }
  };
  return {
    init: init
  };
})();

eyeTracker.init();
