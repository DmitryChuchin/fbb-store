var gulp = require('gulp'),
    sass = require('gulp-sass'),
    pug = require('gulp-pug'),
    browserSync = require('browser-sync').create(),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function () {
  return gulp.src('./src/styles/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'));
});

gulp.task('pug', function() {
    gulp.src('./src/templates/index.pug')
        .pipe(pug({pretty:true}))
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(gulp.dest('./')) // Записываем собранные файлы
});

gulp.task('watch', function() {
  gulp.watch('./src/styles/**/*', ['sass']);
  gulp.watch('./src/templates/index.pug', ['pug']);
})

gulp.task('server', function() {
  browserSync.init({
    server: '.'
  });
  browserSync.watch(['index.html', './css/**/*', './js/**/*']).on('change', browserSync.reload);
});

gulp.task('default', ['sass', 'pug', 'watch', 'server']);
